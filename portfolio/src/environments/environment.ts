// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyDB90Qy2CgS-j7MPSnizXQ2-FYJGpNgrNc",
    authDomain: "angular-portfolio-1258f.firebaseapp.com",
    databaseURL: "https://angular-portfolio-1258f.firebaseio.com",
    projectId: "angular-portfolio-1258f",
    storageBucket: "angular-portfolio-1258f.appspot.com",
    messagingSenderId: "352877270272",
    appId: "1:352877270272:web:22b2ff846da99ab7"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
