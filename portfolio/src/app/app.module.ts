import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

 
import { AppRoutingModule } from './feature/app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './feature/components/navbar/navbar.component';
import { HomeComponent } from './feature/components/home/home.component';
import { AboutComponent } from './feature/components/about/about.component';
import { SkillsComponent } from './feature/components/skills/skills.component';
import { EducationComponent } from './feature/components/education/education.component';
import { ProjectsComponent } from './feature/components/projects/projects.component';
import { HobbiesComponent } from './feature/components/hobbies/hobbies.component';
import { LoginComponent } from './feature/components/login/login.component';


@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    HomeComponent,
    AboutComponent,
    SkillsComponent,
    EducationComponent,
    ProjectsComponent,
    HobbiesComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
